from flask import Flask, Response, abort
from Crypto.Cipher import AES
import base64

app = Flask(__name__)
app.config.from_object('config')


@app.route('/<string:code>')
def get_file_path(code):
    cipher = AES.new(app.config['STORAGE_KEY'].decode('hex'), AES.MODE_ECB)
    try:
        decoded_bytes = base64.urlsafe_b64decode(code.encode('ascii'))
        file_path = cipher.decrypt(decoded_bytes).rstrip('\0')
    except (TypeError, ValueError):
        abort(404)
    record_url = '/internal_record/' + file_path
    headers = {'X-Accel-Redirect': record_url}
    return Response('', headers=headers)

if __name__ == '__main__':
    app.run(debug=True)
